import { ApiRequest } from "../request";

export class RegisterController {
    async register(avatarValue:string, emailValue: string, userNameValue:string, passwordValue: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body({
                avatar: avatarValue,
                email: emailValue,
                userName: userNameValue,
                password: passwordValue,
            })
            
            .send();
        return response;
    }
}
