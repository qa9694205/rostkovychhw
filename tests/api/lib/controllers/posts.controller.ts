import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }
    async createPost(accessToken:string, authorIdValue:number,previewImageValue: string, bodyValue:string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Posts`)
            .body({
                authorId: authorIdValue,
                previewImage: previewImageValue,
                body: bodyValue
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
    async likePost(accessToken:string, entityIdValue:number, isLikeValue:boolean,userIdValue:number, ) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                entityId: entityIdValue,
                isLike: isLikeValue,
                userId: userIdValue
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
    async commentOnPost(accessToken:string, authorIdValue:number,postIdValue:number, bodyValue:string ) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Comments`)
            .body({
                authorId: authorIdValue,
                postId: postIdValue,
                body: bodyValue,
               
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
}