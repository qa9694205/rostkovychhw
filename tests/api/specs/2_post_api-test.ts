const {expect} = require("chai").use(require('chai-json-schema'));
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { checkStatusCode, checkResponseTime, checkSchema } from "../../helpers/functionsForChecking.helper";
import { PostsController } from "../lib/controllers/posts.controller";
const users = new UsersController();
const register = new RegisterController();
const auth = new AuthController();
const posts = new PostsController();
const schemas = require("./data/schemas_testData.json");
describe("Post functionality check", () => {
    let postInfo = [
        { previewImage: "img", body: "Testing create post" },
        { previewImage: "image", body: "Testing create post test test test" },
        { previewImage: "string", body: "Testing create post /1" },
    ];
    let userInfo = { email: "testing99@gmail.com",userName:"user99", password: "password" };
    let accessToken: string;
    let currUserId: number;
    let postId: number;
    before(`Register and login and get the token`, async () => {
        let response = await register.register("", userInfo.email,userInfo.userName, userInfo.password);
 
        checkStatusCode(response, 201);
        checkResponseTime(response,1000);
        checkSchema(response, "schema_register");

        response = await auth.login(userInfo.email, userInfo.password);

        accessToken = response.body.token.accessToken.token;
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkSchema(response, "schema_login");
        currUserId = response.body.user.id;
    });
    postInfo.forEach((post) => {
        it(`Create new post test`, async () => {
            let response = await posts.createPost(accessToken, currUserId, post.previewImage, post.body);
            checkStatusCode(response, 200);
            checkResponseTime(response, 1000);
            checkSchema(response, "schema_singlePost");
            expect(response.body.author.id).to.be.deep.equal(currUserId, "The post's authorId is not correct");
            expect(response.body.body).to.be.deep.equal(post.body, "The post's body is not correct");
            expect(response.body.previewImage).to.be.deep.equal(
                post.previewImage,
                "The post's previewImage is not correct"
            );
            expect(response.body.reactions.length, `Reactions of created post should have 0 items`).to.be.equal(0);
            expect(response.body.comments.length, `Comments of created post should have 0 items`).to.be.equal(0);
            if (postInfo[0] == post) {
                postId = response.body.id;
            }
        });
    });
    it(`View all posts(including just created) test`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkSchema(response, "schema_allPosts");
        response.body.forEach((post) => {
            if (post.id == postId) {
                expect(post.author.id).to.be.deep.equal(currUserId, "The post's authorId is not correct");
                expect(post.body).to.be.deep.equal(postInfo[0].body, "The post's body is not correct");
                expect(post.previewImage).to.be.deep.equal(
                    postInfo[0].previewImage,
                    "The post's previewImage is not correct"
                );
                expect(post.reactions.length, `Reactions of created post should have 0 items`).to.be.equal(0);
                expect(post.comments.length, `Comments of created post should have 0 items`).to.be.equal(0);
            }
        });
    });
    it(`Like created post test`, async () => {
        let response = await posts.likePost(accessToken, postId, true, currUserId);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
    });
    it(`Comment on created post test`, async () => {
        let response = await posts.commentOnPost(accessToken, currUserId, postId, "New comment");
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkSchema(response, "schema_comment");
        expect(response.body.body).to.be.deep.equal("New comment", "Comment body isn't correct");
    });
    it(`View all posts test and check left comments and reactions`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        checkSchema(response, "schema_allPosts");
        response.body.forEach((post) => {
            if (post.id == postId) {
                expect(post.author.id).to.be.deep.equal(currUserId, "The post's authorId is not correct");
                expect(post.body).to.be.deep.equal(postInfo[0].body, "The post's body is not correct");
                expect(post.previewImage).to.be.deep.equal(
                    postInfo[0].previewImage,
                    "The post's previewImage is not correct"
                );
                expect(
                    post.reactions.length,
                    `Reactions of liked post should have more than 0 items`
                ).to.be.greaterThan(0);
                expect(post.reactions[0].user.id).to.be.deep.equal(
                    currUserId,
                    "The like's user id is not correct"
                );
                expect(
                    post.comments.length,
                    `Comments of commented post should have more than 0 items`
                ).to.be.greaterThan(0);
                expect(post.comments[0].author.id).to.be.deep.equal(
                    currUserId,
                    "The comment's user id is not correct"
                );
                expect(post.comments[0].body).to.be.deep.equal("New comment", "The comment's body is not correct");
            }
        });
    });
});
