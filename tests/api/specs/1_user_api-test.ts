const {expect} = require("chai").use(require('chai-json-schema'));
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { RegisterController } from "../lib/controllers/register.controller";
import { checkStatusCode, checkResponseTime, checkSchema } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const register = new RegisterController;
const auth = new AuthController;
const schemas = require('./data/schemas_testData.json');
describe("User functionality check", () => {
    let userInfo = 
        { avatar:"string",email: "testuser@gmail.com",userName:"testuser", password: "password" };
    let incorrectUserInfo = [
        { avatar:"",email: "",userName:"", password: "" },
        { avatar:"",email: "123",userName:"1", password: "123" }
    ];
    let newUserInfo;
    let accessToken: string;
    let currUserId: number;
    before(`Registration positive test`, async () => {
        let response = await register.register(userInfo.avatar, userInfo.email,userInfo.userName, userInfo.password);
 
        checkStatusCode(response, 201);
        checkResponseTime(response,1000);
        checkSchema(response, "schema_register");
        currUserId = response.body.user.id;
    });
    incorrectUserInfo.forEach((user)=>{
        it(`Registration negative test with email: '${user.email}', userName: '${user.userName}' and password: '${user.password}'`, async () => {
            let response = await register.register(user.avatar,user.email, user.userName, user.password);
     
            checkStatusCode(response, 400);
            checkResponseTime(response,1000);
            checkSchema(response, "schema_validationError");
            expect(response.body.errors.length, `There should be at least 3 errors detected`).to.be.greaterThan(2); 
        });
    })
    
    it(`View all users test`, async () => {
        let response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1); 
        checkSchema(response, "schema_allUsers");
        let isInList = false;
        response.body.forEach(user => {
            if(user.id == currUserId){
               isInList = true;
            }
        });
        expect(isInList).to.be.deep.equal(true);
    });
    it(`Login test using registered user`, async () => {
        let response = await auth.login(userInfo.email,userInfo.password);
 
        accessToken = response.body.token.accessToken.token;
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        checkSchema(response, "schema_login");
        expect(response.body.user).to.be.deep.equal({id:currUserId, avatar:userInfo.avatar, email:userInfo.email, userName:userInfo.userName});
    });
    it(`View current user(by token) test`, async () => {
        let response = await users.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        checkSchema(response, "schema_singleUser");
        expect(response.body).to.be.deep.equal({id:currUserId, avatar:userInfo.avatar, email:userInfo.email, userName:userInfo.userName});
    });
    it(`Updating user's data test`, async () => {
        newUserInfo =  { id: currUserId, avatar:"newstring",email: "new12345test@gmail.com",userName:"new12345test" }
        let response = await users.updateUser(newUserInfo, accessToken);
       
        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
    });
    it(`Check the info of updated current user`, async () => {
        let response = await users.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        checkSchema(response, "schema_singleUser"); 
        expect(response.body).to.be.deep.equal(newUserInfo);
    });
    it(`View user's data by id test`, async () => {
        let response = await users.getUserById(currUserId);
        checkStatusCode(response, 200);
        checkResponseTime(response,1000);
        expect(response.body).to.be.deep.equal(newUserInfo, "User details isn't correct");
    });
    it(`Delete user by id`, async () => {
        let response = await users.deleteUser(currUserId, accessToken);
        checkStatusCode(response, 204);
        checkResponseTime(response,1000);
    });
    it(`View user's data by id negative test`, async () => {
        let response = await users.getUserById(currUserId);
        checkStatusCode(response, 404);
        checkResponseTime(response,1000);
        checkSchema(response, "schema_error");
        expect(response.body.error).to.be.deep.equal(`Entity User with id (${currUserId}) was not found.`, "The error message is not correct");
    });
});
